#!/bin/sh

NODE_VERSION=12
REDIS_VERSION=5.0

DIR=/var/www/nodes
echo "----------------- environment for branch $branch --------------------"
case $branch in
	master)
        env='prod'
        location='master'
        dst="$DIR/master"
	;;
	dev)
        env='develop'
        location='dev'
        dst="$DIR/dev"
	;;
	feature/*)
        env='test'
        dst='./build'
	;;
	*)
    	echo "unknown branch $branch"
        exit 1
	;;
esac

echo "---------------------- $env ---------------------------------"

ls -la

#проверяем текущую конфигурацию

programs="yarn redis-cli pm2 curl jq netstat"

for i in $programs
do
    echo -n $i ...
    which $i > /dev/null && echo found || ( echo not found ; exit 1 )
done

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

#nvm> /dev/null 2>&1 || echo nvm not found && exit 1

#check node version
ver=`nvm current | sed -n 's/^v\([[:digit:]]\+\)\..*/\1/p'`
[ -z "$ver" ] && echo unknown node version && exit 1
[ "$ver" -lt $NODE_VERSION ] && echo 'need node version >=' $NODE_VERSION && exit 1

#check redis-server version

if [ -z `docker ps -q -f name=redis` ]
then
    if [ -n `docker ps -q -f name=redis` ]
    then
        docker start redis || exit 1
    else
        docker pull bitnami/redis:$REDIS_VERSION || exit 1
        docker run -d --name redis -p 127.0.0.1:6379:6379 -e ALLOW_EMPTY_PASSWORD=yes  bitnami/redis || exit 1
    fi
#FIXME знаю - грязній хак - пока не придумал поинтереснее.
    sleep 10
fi


ver=`redis-cli info server | awk -F : '/redis_version/{print $2}'`
[ -z "$ver" ] && echo unknown redis-server version && exit 1
echo -e "$REDIS_VERSION\n$ver" | sort -C -V  || ( echo 'need redis-server version >=' $REDIS_VERSION && exit 1 )



echo "=============== deploy $env ===================="
pm2 delete $env
[ X`pm2 jlist | jq -r '.[] | select(.name == "'$env'") | .name'` = 'X'$env ] && echo 'cant delete $env' && exit 1
echo clearing redis...
redis-cli KEYS "$env*" | grep -v '^$' | xargs -r redis-cli DEL
rm -rf $dst || echo''
mkdir -p $dst
tar -zxf build.tgz -C $dst

echo "=================== config for $env =============================="
[ -f config.tgz ] && tar -zxf config.tgz || exit 1

cd config || ( echo missing config dir config && exit 1 )
for i in $env `hostname -s` `hostname -s`-$env `hostname -f` `hostname -f`-$env  local local-$env 
do
    [ -e $i.json ] && cat $i.json || echo '{}'
done | jq -s '.[0] * .[1] * .[2] * .[3] * .[4] * .[5]' - > ../config.json || exit 1
cd ..
cat config/config.json
cp config.json $dst/config

cd $dst

echo "================= patching ecosystem ==========================="

echo patching ecosystem.config.js
echo sed -i "/%ENV%/s/:[^,]\+/:\"$env\"/ig; /NODE_ENV/s/:[^,]\+/:\"config\"/"
[ -e ecosystem.config.js ] && sed -i "/%ENV%/s/:[^,]\+/:\"$env\"/ig; /NODE_ENV/s/:[^,]\+/:\"config\"/" ecosystem.config.js || exit 1

echo ecosystem.config.js
cat ecosystem.config.js

pm2 start
#export NODE_ENV=`pm2 jlist | jq -r '.[] | select(.name == "'$env'") | .pm2_env.NODE_ENV'`
#echo check, is the sane config? $NODE_ENV.json
#cat config/$NODE_ENV.json
echo config/default.json
cat config/default.json
export NODE_ENV=config
yarn run test || exit 1
if [ -n "$location" ]
then
    pid=`pm2 jlist | jq -r '.[] | select(.name == "'$env'") | .pid'`
    port=`netstat -tpln | awk '/ '$pid'\//{print $4}' | tr -d :`
    echo pid $pid port $port
    echo $port | grep -s -P '^\d+$' || exit 1
    echo -n setting nginx upstream $location port:
    [ X`curl -d "server 192.168.120.2:$port;" http://192.168.120.1:8081/upstream/$location` = Xsuccess ] && echo " success" || ( echo 'error' && exit 1 )
    curl https://team:1Zdsx2VKtlBNz04N4gDJ@devops-course-10.echo.dp.ua/$location
fi
