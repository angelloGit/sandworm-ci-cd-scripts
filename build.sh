#!/bin/sh

NODE_VERSION=12

echo "==================== check build environment ================="

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

#nvm> /dev/null 2>&1 || echo nvm not found && exit 1

#check node version
ver=`nvm current | sed -n 's/^v\([[:digit:]]\+\)\..*/\1/p'`
[ -z "$ver" ] && echo unknown node version && exit 1
[ "$ver" -lt $NODE_VERSION ] && echo 'need node version >=' $NODE_VERSION && exit 1

programs="yarn"

for i in $programs
do
    echo -n $i ...
    which $i > /dev/null && echo found || ( echo not found ; exit 1 )
done

cd build

echo "==================== linter ========================"

yarn add jshint
yarn run linter || echo "dont worry. its only linter"

echo  "==================== install modules =========================="

yarn install

echo "====================== artifact =============================== "

tar -zcf ../build.tgz --exclude=.git . 
